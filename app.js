require("./config/database").connect();
const express = require("express");
const bcrypt = require("bcryptjs");
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");

const app = express();

const port = 3000;

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));

// importing user context
const User = require("./model/user");
const Task = require("./model/task");
const Category = require("./model/category");

app.post("/register", async (req, res) => {
  try {
    const { name, username, password } = req.body;

    if (!(name && password && username)) {
      res.status(400).send("All input is required");
    }

    const oldUser = await User.findOne({ username });

    if (oldUser) {
      return res.status(409).send("User Already Exist. Please Login");
    }

    //Encrypt user password
    encryptedPassword = await bcrypt.hash(password, 10);

    // Create user in our database
    const user = await User.create({
      name,
      username,
      password: encryptedPassword,
    });

    // Create token
    const token = jwt.sign({ user_id: user._id, username }, "somethingsecret", {
      expiresIn: "2h",
    });
    // save user token
    user.token = token;

    // return new user
    res.status(201).json(user);
  } catch (err) {
    console.log(err);
  }
  // Our register logic ends here
});

// Login
app.post("/login", async (req, res) => {
  try {
    const { username, password } = req.body;

    if (!(username && password)) {
      res.status(400).send("All input is required");
    }

    const user = await User.findOne({ username });

    if (user && (await bcrypt.compare(password, user.password))) {
      const token = jwt.sign(
        { user_id: user._id, username },
        "somethingsecret",
        {
          expiresIn: "2h",
        }
      );

      console.log("token is : " + token);
      user.token = token;

      res.status(200).json(user);
    }
    res.status(400).send("Invalid Credentials");
  } catch (err) {
    console.log(err);
  }
});

const auth = require("./middleware/auth");

app.post("/welcome", auth, (req, res) => {
  res.status(200).send("Welcome you got the token");
});

app.post("/createtask", auth, async (req, res) => {
  const { username, taskName, categoryName } = req.body;

  const oldTask = await Task.findOne({ taskName });
  if (oldTask) {
    return res
      .status(409)
      .send("This tasks already exists and belongs to a user");
  }

  const findCategory = await Category.findOne({ categoryName });
  if (!findCategory) {
    return res.status(404).send("Category doesn't exist");
  }

  await Task.create({
    username,
    taskName,
    categoryName,
  });

  return res.send("Task added");
});

app.post("/transferexistingtask", auth, async (req, res) => {
  const { username, taskName, assignUser } = req.body;
  // find if the user to whom task will be assigned exists or not
  const user = await User.findOne({ username: assignUser });
  if (!user) return res.status(404).send("User doesn't exist");
  // find if the user who assigns exist or not
  const findTask = await Task.find({ username });
  if (findTask) {
    const taskupdated = await Task.updateOne(
      { taskName },
      { username: assignUser }
    );
    if (taskupdated) {
      return res.send("task has been assigned to the new user");
    } else {
      return res.send("sorry failed to assign task to the new user");
    }
  } else {
    return res.status(404).send("User not authentic");
  }
});
let warning = 0;

app.delete("/removecategorytasks", async (req, res) => {
  if (warning == 0) {
    warning += 1;
    return res.send("Warning!");
  }

  const { username, categoryName } = req.body;
  console.log(username + categoryName);
  const user = await User.find({ username });
  if (user) {
    warning = 0;
    await Task.deleteMany({ username: username, categoryName: categoryName });
    res.send("Deleted successfully");
  } else {
    res.send("User/category doesn't exist");
  }
});

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
