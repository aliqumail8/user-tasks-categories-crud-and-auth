const mongoose = require("mongoose");

const categorySchema = new mongoose.Schema({
  categoryName: { type: String, unique:true }
});

module.exports = mongoose.model("category", categorySchema);