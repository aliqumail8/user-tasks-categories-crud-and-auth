const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
  taskName: { type: String, unique:true },
  username: { type: String, required: true},
  categoryName: { type: String, required: true}
});

module.exports = mongoose.model("task", taskSchema);